const Todo = require("../models/Todo");

const fetchTodos = async (req, res) => {
  try {
    const todos = await Todo.find();
    res.send({ todos });
  } catch (error) {
    res.status(400).send({ message: "Some Think Went Wrong" });
  }
};

const addTodo = async (req, res) => {
  try {
    const todo = new Todo(req.body);
    await todo.save();
    res.send({ todo });
  } catch (error) {
    res.send({ message: error.message });
  }
};

const deleteTodo = async (req, res) => {
  try {
    const _id = req.params.id;
    const todo = await Todo.findOne({ _id });
    if (!todo) {
      return res.state(404).send({ message: "Task Not Found" });
    }
    await todo.remove();
    res.send({ message: "Taks Deleted" });
  } catch (error) {
    res.send({ message: error.message });
  }
};

const updateTodo = async (req, res) => {
  try {
    const _id = req.params.id;
    const todo = await Todo.findOne({ _id });
    if (!todo) {
      return res.state(404).send({ message: "Task Not Found" });
    }
    todo.status = !todo.status;
    await todo.save();
    res.send({ message: "Taks Updated" });
  } catch (error) {
    res.send({ message: error.message });
  }
};

module.exports = {
  fetchTodos,
  addTodo,
  deleteTodo,
  updateTodo,
};
