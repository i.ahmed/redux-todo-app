const express = require("express");
const todoRouter = require("./routes/todo");
require("./config/db");
const app = express();
app.use(express.json());
app.use("/api", todoRouter);
app.listen(process.env.PORT || 5000, () =>
  console.log(`running on port ${process.env.PORT || 5000}`)
);
