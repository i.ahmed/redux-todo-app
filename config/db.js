const mongoose = require("mongoose");

mongoose.connect(
  process.env.MONGODB_URL || "mongodb://127.0.0.1:27017/todo-app",
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    autoIndex: true,
  }
);
