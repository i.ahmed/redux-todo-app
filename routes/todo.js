const express = require("express");
const {
  fetchTodos,
  addTodo,
  deleteTodo,
  updateTodo,
} = require("../controllers/todoController");
const router = express.Router();
router.get("/todos", fetchTodos);
router.post("/todos", addTodo);
router.patch("/todos/:id", updateTodo);
router.delete("/todos/:id", deleteTodo);

module.exports = router;
