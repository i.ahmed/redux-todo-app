import "./App.css";
import TodoContainer from "./componant/TodoContainer";

function App() {
  return (
    <div className="App">
      <TodoContainer />
    </div>
  );
}

export default App;
