import {
  FETCH_TODOS_SUCCESS,
  FETCH_TODOS_REQUEST,
  FETCH_TODOS_FALIUER,
  ADD_TODO_SUCCESS,
  ADD_TODO_REQUEST,
  ADD_TODO_FALIUER,
  DELETE_TODO_SUCCESS,
  DELETE_TODO_REQUEST,
  DELETE_TODO_FALIUER,
  UPDATE_TODO_FALIUER,
  UPDATE_TODO_SUCCESS,
  UPDATE_TODO_REQUEST,
} from "./todoTypes";

const initState = {
  loading: false,
  todos: [],
  error: "",
};

const todoReducer = (state = initState, action) => {
  let todos = [];
  switch (action.type) {
    case FETCH_TODOS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_TODOS_SUCCESS:
      return {
        ...state,
        loading: false,
        todos: action.payload,
      };
    case FETCH_TODOS_FALIUER:
      return {
        ...state,
        loading: false,
        todos: [],
        error: action.payload,
      };
    case ADD_TODO_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ADD_TODO_SUCCESS:
      todos = state.todos;
      todos.push(action.payload);
      return {
        ...state,
        loading: false,
        todos,
        error: "",
      };
    case ADD_TODO_FALIUER:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case DELETE_TODO_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case DELETE_TODO_SUCCESS:
      todos = state.todos.filter((todo) => todo._id !== action.payload);
      return {
        ...state,
        loading: false,
        todos,
        error: "",
      };
    case DELETE_TODO_FALIUER:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case UPDATE_TODO_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_TODO_SUCCESS:
      todos = state.todos.map((todo) => {
        if (todo._id === action.payload) {
          return { ...todo, status: !todo.status };
        }
        return todo;
      });
      return {
        ...state,
        loading: false,
        todos,
        error: "",
      };
    case UPDATE_TODO_FALIUER:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default todoReducer;
