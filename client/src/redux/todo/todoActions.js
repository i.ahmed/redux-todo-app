import axios from "axios";
import * as todoActionTypes from "./todoTypes";

export const fetchTodosRequest = () => {
  return {
    type: todoActionTypes.FETCH_TODOS_REQUEST,
  };
};

export const fetchTodosSuccess = (todos) => {
  return {
    type: todoActionTypes.FETCH_TODOS_SUCCESS,
    payload: todos,
  };
};

export const fetchTodosFaliuer = (error) => {
  return {
    type: todoActionTypes.FETCH_TODOS_FALIUER,
    payload: error,
  };
};

export const addTodoRequest = () => {
  return {
    type: todoActionTypes.ADD_TODO_REQUEST,
  };
};

export const addTodoSuccess = (todo) => {
  return {
    type: todoActionTypes.ADD_TODO_SUCCESS,
    payload: todo,
  };
};

export const addTodoFaliuer = (error) => {
  return {
    type: todoActionTypes.ADD_TODO_FALIUER,
    payload: error,
  };
};

export const deleteTodoRequest = () => {
  return {
    type: todoActionTypes.DELETE_TODO_REQUEST,
  };
};

export const deleteTodoSuccess = (_id) => {
  return {
    type: todoActionTypes.DELETE_TODO_SUCCESS,
    payload: _id,
  };
};

export const deleteTodoFaliuer = (error) => {
  return {
    type: todoActionTypes.DELETE_TODO_FALIUER,
    payload: error,
  };
};

export const updateTodoRequest = () => {
  return {
    type: todoActionTypes.UPDATE_TODO_REQUEST,
  };
};

export const updateTodoSuccess = (_id) => {
  return {
    type: todoActionTypes.UPDATE_TODO_SUCCESS,
    payload: _id,
  };
};

export const updateTodoFaliuer = (error) => {
  return {
    type: todoActionTypes.UPDATE_TODO_FALIUER,
    payload: error,
  };
};

export const fetchTodos = () => {
  return (dispatch) => {
    dispatch(fetchTodosRequest());
    axios
      .get("/api/todos")
      .then((response) => {
        dispatch(fetchTodosSuccess(response.data.todos));
      })
      .catch((error) => {
        dispatch(fetchTodosFaliuer(error.message));
      });
  };
};

export const addTodo = (newTodo) => {
  return (dispatch) => {
    dispatch(addTodoRequest());
    axios
      .post("/api/todos", newTodo)
      .then((response) => {
        console.log(response);
        dispatch(addTodoSuccess(response.data.todo));
      })
      .catch((error) => {
        dispatch(addTodoFaliuer(error.message));
      });
  };
};
export const deleteTodo = (_id) => {
  return (dispatch) => {
    dispatch(deleteTodoRequest());
    axios
      .delete(`/api/todos/${_id}`)
      .then((response) => {
        dispatch(deleteTodoSuccess(_id));
      })
      .catch((error) => {
        dispatch(deleteTodoFaliuer(error.message));
      });
  };
};
export const updateTodo = (_id) => {
  console.log(_id);
  return (dispatch) => {
    dispatch(updateTodoRequest());
    axios
      .patch(`/api/todos/${_id}`)
      .then((response) => {
        dispatch(updateTodoSuccess(_id));
      })
      .catch((error) => {
        dispatch(updateTodoFaliuer(error.message));
      });
  };
};
