export const todos = [
  {
    _id: 1,
    title: "learn redux",
    status: true,
  },
  {
    _id: 2,
    title: "build todo app with redux",
    status: false,
  },
];
