import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { FcFullTrash } from "react-icons/fc";
import {
  addTodo,
  fetchTodos,
  deleteTodo,
  updateTodo,
} from "../redux/todo/todoActions";
const TodoContainer = ({
  error,
  todos,
  loading,
  fetchTodos,
  addTodo,
  deleteTodo,
  updateTodo,
}) => {
  const todoRef = useRef();
  useEffect(() => {
    fetchTodos();
  }, []);
  const handleClick = () => {
    const title = todoRef.current.value.trim();
    if (title !== "") {
      addTodo({ title });
      todoRef.current.value = "";
    }
  };
  return (
    <section className="wrapper">
      <article className="todo-wrapper">
        <header>
          <h4>My Todo list</h4>
          <div className="input-wrapper">
            <input
              type="text"
              placeholder="What do you need to do today?"
              ref={todoRef}
            />
            <button onClick={handleClick}>Add</button>
          </div>
        </header>
        <div className="todo-list">
          {loading ? (
            <div className="spinner-container">
              <div className="sk-chase">
                <div className="sk-chase-dot"></div>
                <div className="sk-chase-dot"></div>
                <div className="sk-chase-dot"></div>
                <div className="sk-chase-dot"></div>
                <div className="sk-chase-dot"></div>
                <div className="sk-chase-dot"></div>
              </div>
            </div>
          ) : error ? (
            <p>{error}</p>
          ) : (
            <ul>
              {todos &&
                todos.length > 0 &&
                todos.map((todo) => {
                  const { _id, status, title } = todo;
                  return (
                    <li key={_id} className={`${status ? "compeleted" : ""}`}>
                      <label htmlFor="status">
                        {status ? (
                          <input
                            type="checkbox"
                            id="status"
                            checked
                            onChange={() => updateTodo(_id)}
                          />
                        ) : (
                          <input
                            type="checkbox"
                            id="status"
                            onChange={() => updateTodo(_id)}
                          />
                        )}
                        {title}
                      </label>
                      <span onClick={() => deleteTodo(_id)}>
                        <FcFullTrash fontSize={"1.5rem"} />
                      </span>
                    </li>
                  );
                })}
            </ul>
          )}
        </div>
      </article>
    </section>
  );
};

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodos: () => dispatch(fetchTodos()),
    addTodo: (newTodo) => dispatch(addTodo(newTodo)),
    deleteTodo: (_id) => dispatch(deleteTodo(_id)),
    updateTodo: (_id) => dispatch(updateTodo(_id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TodoContainer);
